---
title: "about shorts"
tags: ["about", "shorts"]
date: 2021-09-24
draft: false
---

shorts are simply small posts.

they may be reminders to write more about the topic, or as a place to quickly store additional thoughts on a topic.