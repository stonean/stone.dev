---
title: "mage"
date: 2021-09-17
tags: ["maker", "go", "automation"]
draft: false
---

[mage](https://magefile.org/) is "a make/rake-like build tool using Go."

the initial idea to get Mage in place was twofold:

- we wanted a cross-platform system like makefile that would be stupid-easy for the dev team to use.
- we also wanted a foundation we could extend to address needs we haven't thought of yet.

that was it, pretty simple. cross-platform and extendable.

we quickly added a series of pre-commit hooks, with help from [husky](https://www.npmjs.com/package/husky), to address the recurring merge request (pull request) issues - saving valuable time for all involved.

the [zero install option](https://magefile.org/zeroinstall/) for mage lets us manage the project's dependencies in the project's [go.mod](https://golang.org/ref/mod) file.

while not the shortest command, `go run mage.go <target>`, is easily supportable across all operating systems Go supports.

mage has turned out to be a great cross-platform solution for task scripting.
