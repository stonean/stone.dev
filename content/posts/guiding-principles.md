---
title: "guiding principles"
date: 2020-08-28
tags: ["maker", "manager", "philosophy"]
draft: false
---

brad mohs and i worked on these principles for several weeks attempting to strike the right balance of terms and coverage of concerns.

i'm quite happy where we landed on this, but i do expect this to evolve over time.

if changes are made, i'll update this post with those changes and thoughts behind them.

## technology guiding principles

* scalable: design and implement to be dynamically scaled
* learnable: short time to meaningful contribution for new team members including: documentation needed for self motivated engagement
* reliable: system continues to operate properly in the event of one or more component failures
* recordable: data recorded to provide meaningful business metrics and playback
* supportable: simple and quick to get from issue notification to identification to resolution
* automated: Humans only do what computers can’t
* testable: design for unit, functional, security and load testing
* consumable: interfaces into our systems will be simple and intuitive


## business guiding principles

* secure: pci compliance and pii best practices
* fast: systems. time to market. updates. bug fixes
* serviceable: built to be used
* evolvable: adapt, grow and create products/services as the business needs
* malleable: products/services/outputs fit the varied needs of our customers/clients
* observable: the business must know how our systems are performing: accurate sales reporting, insightful system monitoring and meaningful notification system
* reviewable: ability to visually confirm before promoting to production.
multi-tenant: single instance serves multiple tenants