---
title: "filters"
date: 2021-09-24
tags: ["ux"]
draft: false
---

i was recently searching for info related to eslint, hugo, vscode, and jsonfig.json - a post will appear soon with the problem and solution.

what i found frustrating was the number of sites littering results with only a small snippet of code, little-to-no description, but a bunch of ads. it is possible to remove site(s) from _a_ search with `-site: <sitname>`, but there isn't a permanent setting for exclusions.

exclusions are very useful at both the site and keyword (key phrase) levels. i think this applies to most search use-cases, not just google ones. job searches to exclude technologies you don't want to use, or red-flag phrases such as "fast-paced environment" sounds like a good idea. grocery searches to exclude certain ingredients would be helpful for those with allergies. i can see people wanting to exclude compaines from product searches for poor quality, poor support, or other personal reasons.

please consider adding this to your search settings.