---
title: "bark intro"
date: 2021-08-14
tags: ["maker", "bark"]
draft: false
---

about three years ago, i started working on a new programming language syntax.  every few months or so, i would revisit the readme document and make some modifications - mainly as a mental reset for another topic.

as with all my side distractions, i needed a name - a word to reference when revisiting the idea. the result was [bark](https://gitlab.com/bark-lang/bark).

with development started on [bark](https://gitlab.com/bark-lang/bark), i found myself wanting to write down my thoughts as i add, remove, modify, and re-add code.  i have no idea what i'm doing with this and am learning by reading the source code of projects like [go](https://github.com/golang/go) and others listed in the readme references section.

regarding references, i want to thank [thorsten ball](https://thorstenball.com/) for his books - ["writing an interpreter in go"](https://interpreterbook.com/) and ["writing a compiler in go."](https://compilerbook.com/) this project would not have started without these books. thank you.

to be very clear, don't mistake this for a claim that bark is anything more than some thoughts jotted down in a readme doc. that's essentially all it is.

this post exists to open the door for future posts. i wanted to share more thoughts than a commit message should contain. the context of a commit message isn't sufficient to convey the evolving ideas regarding bark's place in the world. core facets of bark's identity are gaining clarity.

expect curveballs if you follow along.
