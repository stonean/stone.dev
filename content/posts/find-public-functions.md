---
title: "find public functions"
tags: ["maker", "go"]
date: 2021-10-26
draft: false
---

no particular need on this one, simple curiosity.

to find public functions in vs code for go:

- select `Aa` in the search bar to match case
- select `.*` in the search bar to use a regular expression
- paste this in the search bar:
 > `^func\s(\([a-zA-Z\s\*\.]+\))?\s[A-Z]`

the end.
