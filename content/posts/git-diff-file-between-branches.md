---
title: "git diff file between branches"
tags: ["maker", "go"]
date: 2022-01-08
draft: false
---

if you're on a feature branch and want to diff a file against another branch, main in this example:

```bash
git diff main -- path/to/file
```

examples:

```bash
git diff main -- Makefile
git diff main -- cmd/app/main.go
```

the end.
